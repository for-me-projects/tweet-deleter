package limitless.tweetdeleter.Other.Model;

public class ModelSelect {

    private boolean selected;

    public ModelSelect(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
